import {Apis, ChainConfig} from "../lib/ws";
import ChainStore from "../lib/chain/src/ChainStore";
import {environment} from "./environments/environment";
import PrivateKey from "../lib/ecc/src/PrivateKey"
import TransactionBuilder from "../lib/chain/src/TransactionBuilder"
const {FetchChain} = ChainStore;

export default class DoreService {

    constructor() {
        ChainConfig.networks.decent = {
            chain_id: environment.chain_id
        };
        this.api = this._connectCoreApi();
    }

    /**
     * @param addressIndex
     * @returns {*}
     * @private
     */
    _getAddress(addressIndex = 0)  {
        if (environment.decent_network_wspaths.length <= addressIndex) {
            return false;
        }
        return environment.decent_network_wspaths[addressIndex];
    };

    _connectCoreApi (addressIndex = 0)  {
        const coreApiObject = Apis.instance(this._getAddress(), true);
        return coreApiObject.init_promise
            .then(res => {
                // get current account here and check accountObj is init or not.
                this.chainStore = ChainStore.init()
                    .catch((err) => {
                        console.log('err==========');
                        console.error(err);
                        if (err.message.startsWith('ChainStore sync error')) {
                            console.error('Time synchronisation error, set your date and time');
                        }
                    });
            })
            .catch(e => {
                console.log(e);
                coreApiObject.close();
                if (environment.decent_network_wspaths.length >= addressIndex) {
                    this._connectCoreApi(addressIndex + 1);
                } else {
                    console.error('system_error');
                }
            });
    };

    getAccount (account) {
        let _self = this;
        return new Promise((resolve, reject) => {
            _self.api.then((res) => {
                FetchChain('getAccount', account).then(acc => {
                    resolve(acc);
                });
            })
        });
    };

    /**
     * vote (update account)
     * @returns {Promise<any>}
     */
    update (authObj, memoKey) {
        let _self = this;
        return new Promise((resolve, reject) => {
            const pKey = PrivateKey.fromWif(authObj.pKey);
            _self.api.then(() => {
                _self.chainStore.then(async () => {
                    const feeObj = {
                        amount: 500000,
                        asset: '1.3.0'
                    };
                    Promise.resolve(1).then(res => {
                        const operationObj = {
                            fee: {
                                amount: feeObj.amount,
                                asset_id: feeObj.asset
                            },
                            account: authObj.id,
                            new_options: {
                                memo_key: memoKey,
                                voting_account: '1.2.3',
                                votes: [],
                                allow_subscription: false,
                                price_per_subscribe: {amount: 0, asset_id: '1.3.0'},
                                num_miner: 0,
                                num_witness: 0,
                                subscription_period: 0
                            },
                        };
                        console.log(operationObj);
                        const tr = new TransactionBuilder();
                        tr.add_type_operation('account_update', operationObj);
                        tr.set_required_fees().then(() => {
                            tr.add_signer(pKey, pKey.toPublicKey().toPublicKeyString());
                            tr.broadcast(() => {
                                console.log('It is going on broadcast.');
                                _self.getAccount(authObj.auth).then(acc => {
                                    // get account
                                    console.log(acc.toJS());
                                });
                                resolve(true);
                            }).catch(err => {
                                reject(err);
                            });
                        }).catch(err => {
                            reject(err);
                        })
                    });
                });
            });
        });
    };
}
